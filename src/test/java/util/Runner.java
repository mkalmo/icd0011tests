package util;

import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import tests.*;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Runner {

    public static void main(String[] args) {

        String tag;

        if (args.length < 1) {
            throw new RuntimeException("Provide tag as an argument");
        } else {
            tag = args[0];
        }

        if (args.length >= 2) {
            AbstractHw.setPathToSourceCode(args[1]);
        }

        if (args.length >= 3) {
            AbstractHw.setArg1(args[2]);
        }

        new Runner().run(tag);
    }

    private void run(String tag) {

        PrintStream out = System.out;

        System.setOut(new PrintStream(new NullOutputStream()));

        JUnitCore junit = new JUnitCore();
        junit.addListener(new RunListener() {
            @Override
            public void testFailure(Failure failure) {

                out.println("   " + failure.getDescription() + " failed");
                out.println(failure.getException());

                String trace = Arrays.stream(failure.getException().getStackTrace())
                        .filter(f -> ! f.getClassName().startsWith("junit."))
                        .filter(f -> ! f.getClassName().startsWith("jdk."))
                        .filter(f -> ! f.getClassName().startsWith("java."))
                        .filter(f -> ! f.getClassName().startsWith("org."))
                        .filter(f -> ! f.getClassName().startsWith("util.Runner"))
                        .filter(f -> ! f.getClassName().startsWith("tests.AbstractHw"))
                        .map(StackTraceElement::toString)
                        .collect(Collectors.joining("\n"));

                out.println(trace);

                out.println("\n\n");
            }
        });

        var result = junit.run(resolveClass(tag));

        if (result.wasSuccessful()) {
            out.println("RESULT: PASSED");
        } else {
            out.println("RESULT: FAILED");
        }
    }

    private static Class<?> resolveClass(String tag) {
        if (!List.of(
                "hw01a", "hw01b", "hw02", "hw03", "hw03a", "hw04", "hw05", "hw05a",
                "hw06", "hw06a", "hw07", "hw07a", "hw08",
                "hw09", "hw10", "hw10a").contains(tag)) {

            throw new IllegalStateException("unknown tag: " + tag);
        }

        return loadClass("tests.Hw" + tag.substring(2));
    }

    private static Class<?> loadClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}